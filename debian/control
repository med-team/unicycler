Source: unicycler
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Liubov Chuprikova <chuprikovalv@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               default-jdk <!nocheck>,
               bcftools,
               bowtie2 <!nocheck>,
               miniasm,
               ncbi-blast+ <!nocheck>,
               pilon <!nocheck>,
               racon,
               samtools <!nocheck>,
               spades (>= 3.14),
               libseqan2-dev,
               zlib1g-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/unicycler
Vcs-Git: https://salsa.debian.org/med-team/unicycler.git
Homepage: https://github.com/rrwick/Unicycler
Rules-Requires-Root: no

Package: unicycler
Architecture: any
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         default-jre,
         bcftools,
         bowtie2,
         miniasm,
         ncbi-blast+,
         pilon,
         racon,
         samtools,
         spades (>= 3.14)
Recommends: unicycler-data
Description: hybrid assembly pipeline for bacterial genomes
 Unicycler is an assembly pipeline for bacterial genomes. It can assemble
 Illumina-only read sets where it functions as a SPAdes-optimiser. It can
 also assembly long-read-only sets (PacBio or Nanopore) where it runs a
 miniasm+Racon pipeline. For the best possible assemblies, give it both
 Illumina reads and long reads, and it will conduct a hybrid assembly.

Package: unicycler-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: hybrid assembly pipeline for bacterial genomes (data package)
 Unicycler is an assembly pipeline for bacterial genomes. It can assemble
 Illumina-only read sets where it functions as a SPAdes-optimiser. It can
 also assembly long-read-only sets (PacBio or Nanopore) where it runs a
 miniasm+Racon pipeline. For the best possible assemblies, give it both
 Illumina reads and long reads, and it will conduct a hybrid assembly.
 .
 This package contains architecture independent data for unicycler.
