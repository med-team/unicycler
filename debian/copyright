Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Unicycler
Source: https://github.com/rrwick/Unicycler/releases
Files-Excluded: */seqan

Files: *
Copyright: 2016-2018 Ryan R. Wick, Louise M. Judd, Claire L. Gorrie, Kathryn E. Holt
License: GPL-3+

Files: unicycler/*/miniasm/*
Copyright: © 2015 Broad Institute <hengli@broadinstitute.org>
License: MIT
Comment: Debian ships a miniasm package which is based on tagged miniasm
         releases while this code here is based on latest Git commit
         Asked for clarification in issue
           https://github.com/lh3/miniasm/issues/51

Files: unicycler/include/miniasm/khash.h
       unicycler/include/miniasm/kseq.h
       unicycler/include/miniasm/ksort.h
       unicycler/include/miniasm/kvec.h
Copyright: © 2008, 2009, 2011 Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: unicycler/*/minimap/*
Copyright: © 2015 Broad Institute <hengli@broadinstitute.org>
License: MIT
Comment: Debian ships a minimap package which is based on tagged minimap
         releases while this code here is based on latest Git commit

Files: unicycler/include/minimap/khash.h
       unicycler/include/minimap/kseq.h
       unicycler/include/minimap/ksort.h
       unicycler/include/minimap/kvec.h
Copyright: © 2008, 2009, 2011 Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: debian/*
Copyright: 2018 Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

